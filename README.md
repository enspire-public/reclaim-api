# Reclaim API

This project contains a Swagger 2.0 specification for the Enspire integration with Reclaim.

## Using the API specification

The Swagger 2.0 specification is a formatted file that describes the HTTP API interface. This file can be used to
generate documentation and API client code. There are many open source code generation projects that use Swagger 2.0 or
OpenAPI formats. The [go-swagger](https://github.com/go-swagger/go-swagger) project is recommended for Golang clients.

You can automatically transform this specification to OpenAPI 3 format
using [https://editor.swagger.io/](https://editor.swagger.io/).

Please view the generated documentation page by viewing the GitLab page for the `swagger.yml` file.
